package ru.micke.tasks;

import ru.micke.utils.IOStream;

import java.util.*;

public class T1StringCounter {
    public static void countWords(List<String> FileText) {
        Iterator<String> iterator = FileText.iterator();
        Set <String> finalSet = new TreeSet<String>();

        StringTokenizer tokenizer;
        while (iterator.hasNext()) {
            tokenizer = new StringTokenizer( iterator.next().toLowerCase()
                                           , " ,.:;\"|\\/-=+*-!?#$%&()~<>");
            while (tokenizer.hasMoreTokens()) {
                finalSet.add(tokenizer.nextToken());
            }
        }
        IOStream.message("Общее кол-во слов:" + finalSet.size());
    }

}
