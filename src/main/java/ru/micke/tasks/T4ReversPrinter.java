package ru.micke.tasks;

import ru.micke.utils.IOStream;

import java.util.List;
import java.util.ListIterator;

public class T4ReversPrinter {
    public static void printInReverseOrder(List<String> fileText) {
        ListIterator<String> stringListIterator = fileText.listIterator(fileText.size());
        while (stringListIterator.hasPrevious()){
            IOStream.message(stringListIterator.previous());
        }
    }
}
