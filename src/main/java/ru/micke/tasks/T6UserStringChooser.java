package ru.micke.tasks;

import ru.micke.utils.IOStream;

import java.util.ArrayList;
import java.util.List;

public class T6UserStringChooser {
    public static void printStringByUserChoose(List<String> FileText) {
        ArrayList<Integer> numbers = inputData();
        numbers.forEach(number-> printStringNumberFrom(number,FileText) );
    }
    private static ArrayList<Integer> inputData() {
        ArrayList <Integer> list = new ArrayList <Integer>();
        Integer newNumb = null;

        String str = inputNumbStr();
        while (str.contains("print") == false) {
            try {
                newNumb = Integer.parseInt(str);
            } catch (NumberFormatException exp){
                IOStream.message("Ошибка ввода");
                str = inputNumbStr();
                continue;
            }
            list.add(newNumb);
            str = inputNumbStr();
        }
        return list;
    }
    private static String inputNumbStr(){
        IOStream.message("Введите номер строки для вывода или print для вывода");
        return IOStream.inputString();
    }
    private static void printStringNumberFrom(Integer numb,List<String> FileText ){
        if(numb >0 && numb < FileText.size() ){
            IOStream.message( FileText.listIterator(numb).previous());
        } else {
            IOStream.message("Ошибка номера строки");
        }
    }
}
