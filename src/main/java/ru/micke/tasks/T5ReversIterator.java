package ru.micke.tasks;

import ru.micke.utils.ReverseIterator;

import java.util.List;

public class T5ReversIterator {
    public static void printReverseByIterator(List <String> list){
        ReverseIterator ri = new ReverseIterator(list);
        while (ri.hasNext()){
            System.out.println(ri.next());
        }
    }
}
