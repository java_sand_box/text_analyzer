package ru.micke.tasks;

import java.util.*;

public class T3WordsStats {
    public static void printStatistics(List<String> FileText){
        List<String> words = getWordsList(FileText);

        Map<String, Integer> data = new HashMap<String, Integer>();
        Iterator<String> iterator = words.iterator();

        while (iterator.hasNext()){
            String key  = iterator.next();
            Integer val = data.get(key);
            data.put(key, val == null ? 1 : val +1);
        }

        for( Map.Entry<String, Integer> entry : data.entrySet()){
            System.out.println(entry.getValue() + ": " + entry.getKey());
        }
    }
    public static List<String> getWordsList(List<String> FileText){
        Iterator<String> iterator = FileText.iterator();
        List <String> finalList = new LinkedList<String>();

        StringTokenizer tokenizer;
        while (iterator.hasNext()) {
            tokenizer = new StringTokenizer( iterator.next().toLowerCase()
                    , " ,.:;\"|\\/-=+*-!?#$%&()~<>");
            while (tokenizer.hasMoreTokens()) {
                finalList.add(tokenizer.nextToken());
            }
        }
        return finalList;
    }
}
