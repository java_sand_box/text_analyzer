package ru.micke.tasks;

import ru.micke.utils.StringLenComparator;

import java.util.*;

public class T2SortedWords {
    public static void printWordsSortedByLen(List<String> fileText){
        Iterator<String> iterator = fileText.iterator();

        Set<String> finalSet = new TreeSet<String>(
                                   new StringLenComparator().thenComparing(
                                           Comparator.naturalOrder()));
        StringTokenizer tokenizer;
        while (iterator.hasNext()) {
            tokenizer = new StringTokenizer( iterator.next().toLowerCase()
                    , " ,.:;\"|\\/-=+*-!?#$%&()~<>");
            while (tokenizer.hasMoreTokens()) {
                finalSet.add(tokenizer.nextToken());
            }
        }
        System.out.println("размер:" + finalSet.size());
        System.out.println(finalSet);
    }
}
