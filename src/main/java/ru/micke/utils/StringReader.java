package ru.micke.utils;

import java.util.List;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;

public class StringReader {
    public static List <String> readFile(String filename) throws IOException {
        List<String> strings = Files.readAllLines(Paths.get(filename), UTF_8);
        for(String s: strings){
            IOStream.message(s);
        }
        return strings;
    }
}
