package ru.micke.utils;

import java.util.Iterator;
import java.util.List;

public class ReverseIterator implements Iterator , Iterable {
    private final List _list;
    private int _pos;

    public ReverseIterator (List list){
        _list = list;
        _pos = _list.size()-1;
    }

    @Override
    public Iterator iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return _pos >=0;
    }

    @Override
    public Object next() {
        return _list.get(_pos--);
    }
}
