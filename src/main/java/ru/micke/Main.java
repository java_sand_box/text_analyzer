package ru.micke;
import ru.micke.tasks.*;
import ru.micke.utils.StringReader;
import ru.micke.utils.TestUtils;

import java.io.IOException;
import java.util.List;

import static ru.micke.utils.IOStream.*;

public class Main {

    public static void main(String[] args) throws IOException {
        List <String> FileText = StringReader.readFile(
                                               TestUtils.getConstFileName());
        boolean isRunning = true;
        while (isRunning) {
            message("Введите номер задания или 0 для выхода");
            message("Задание 1: Подсчитайте количество различных слов в файле.");
            message("Задание 2: Выведите на экран список различных слов файла, " +
                                         "отсортированный по возрастанию их длины.");
            message("Задание 3: Подсчитайте и выведите на экран сколько раз " +
                                         "каждое слово встречается в файле.");
            message("Задание 4: Выведите на экран все строки файла в обратном порядке.");
            message("Задание 5: Реализуйте свой Iterator для обхода списка в обратном порядке.");
            message("Задание 6: Выведите на экран строки, номера которых задаются " +
                                         "пользователем в произвольном порядке");

            switch (inputInt()){
                case 0: isRunning = false;
                        break;
                case 1: T1StringCounter.countWords(FileText);
                        break;
                case 2: T2SortedWords.printWordsSortedByLen(FileText);
                        break;
                case 3: T3WordsStats.printStatistics(FileText);
                        break;
                case 4: T4ReversPrinter.printInReverseOrder(FileText);
                        break;
                case 5: T5ReversIterator.printReverseByIterator(FileText);
                        break;
                case 6: T6UserStringChooser.printStringByUserChoose(FileText);
                        break;
                default: message("Введён неверный номер задания, повторите ввод");
            }
            message("----------------------------------------------------------------------------");
        }

    }

}
